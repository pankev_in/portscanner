import javax.swing.*;
import java.lang.reflect.Array;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        String IP = JOptionPane.showInputDialog("IP Address");
        int portStart = Integer.parseInt(JOptionPane.showInputDialog("Start Port Nummber"));
        int portEnd = Integer.parseInt(JOptionPane.showInputDialog("End Port Nummber"));
        List<Integer> openPorts = new ArrayList<Integer>();
        System.out.println(IP + " portStart:"+portStart+ "portEnd:"+portEnd);
        System.out.println("Start scanning...");
        for(int i = portStart; i<=portEnd;i++){
            boolean tf = tryport(IP,i);
            if (tf){openPorts.add(i);}
        }
        System.out.println("Done Scanning");
        System.out.println(openPorts);
    }

    private static boolean tryport(String ip, int port){
        try{
            Socket trySocket = new Socket(ip, port);
            return true;
        }catch(Exception e) {
            return false;
        }
    }
}
